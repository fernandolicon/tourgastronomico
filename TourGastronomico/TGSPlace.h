//
//  TSGPlace.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 16/3/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBGHTMLEntityDecoder.h"
@class TGSCity;

@interface TGSPlace : NSObject

@property NSString *name;
@property int idPlace;
@property NSString *address;
@property NSString *phoneNumber;
@property NSString *schedule;
@property NSURL *googleMapsLocation;
@property NSURL *googleMapsURL;
@property NSString *suggestions;
@property NSString *descriptionLocation;
@property NSString *engDescription;
@property NSString *infoDetail;
@property NSMutableArray *imageURLs;
@property NSInteger rating;
@property bool hasDetails;

- (instancetype) initWithDictionary: (NSDictionary *) responseObject;
- (void) populateBasicInfofwithDictionary: (NSDictionary *) responseObject;
- (void) populateDetailswithDictionary: (NSDictionary *) responseObject;
- (void) populateDetailswithDictionaryforHotel: (NSDictionary *) responseObject;

- (NSString *) descriptionInfo;
- (NSString *) descriptionDetail;

@end
