//
//  TGSDetailPlaceViewController.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 23/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "TGSDetailPlaceViewController.h"
@class TGSRestaurantDetailViewController;

@interface TGSDetailPlaceViewController ()

@end

@implementation TGSDetailPlaceViewController

@synthesize detail;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    detailTxt.text = detail;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)returnToView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
