//
//  TGSImageFullScreenViewController.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 23/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "TGSImageFullScreenViewController.h"

@interface TGSImageFullScreenViewController ()

@end

@implementation TGSImageFullScreenViewController

@synthesize imageURLs;
@synthesize imagePage;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    imagePager.pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    [imagePager setImageCounterDisabled:YES];
    [imagePager setCurrentPage:imagePage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Image Pager Methods

- (NSArray *) arrayWithImages:(KIImagePager *)pager{
        return imageURLs;
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager*)pager
{
    [imagePager setCurrentPage:imagePage];
    return UIViewContentModeScaleAspectFill;
}

- (IBAction)returnToView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
