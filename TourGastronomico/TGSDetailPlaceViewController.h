//
//  TGSDetailPlaceViewController.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 23/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGSDetailPlaceViewController : UIViewController{
    __weak IBOutlet UITextView *detailTxt;
}

@property (strong, nonatomic) NSString *detail;

- (IBAction)returnToView:(id)sender;

@end
