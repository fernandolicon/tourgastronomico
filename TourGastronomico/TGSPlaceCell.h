//
//  TGSPlaceCellTableViewCell.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 20/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TGSPlaceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *placeButton;

@end
