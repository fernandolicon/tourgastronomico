//
//  TGSPlaceCellTableViewCell.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 20/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "TGSPlaceCell.h"

@implementation TGSPlaceCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
