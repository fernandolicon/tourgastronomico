//
//  main.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 13/3/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
