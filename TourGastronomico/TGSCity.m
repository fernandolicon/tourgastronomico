//
//  TGSCity.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 16/3/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

/*
    Cities ID:
        1 - Chihuahua
        10 - Juarez
        11 - Cuauhtemoc
        12 - Delicias
 */

#import "TGSCity.h"

@implementation TGSCity

- (instancetype) initWithDictionary: (NSDictionary *) cityDictionary{
    if (self = [super init]){
        self.name = [cityDictionary objectForKey:@"nombre"];
        self.idCity = [[cityDictionary objectForKey:@"id"] integerValue];
    }
    
    return self;
}

+ (TGSCity *) userPreferencesCity{
    NSString *city = (NSString*) [[NSUserDefaults standardUserDefaults] objectForKey:@"cityName"];
    NSString *cityId = (NSString*) [[NSUserDefaults standardUserDefaults] objectForKey:@"idCity"];
    TGSCity *userCity = [[TGSCity alloc] init];
    
    if (city != nil){
        userCity.name = city;
        userCity.idCity = [cityId integerValue];
    }
    
    return userCity;
}

- (NSURL *) bannerURL{
    NSURL *bannerURL;
    NSString *stringURL;
    
    switch (self.idCity) {
        case 1:
            stringURL = [NSString stringWithFormat:@"%@banner/2.png", tourGastronomicoWSBaseUrl];
            break;
        case 10:
            stringURL = [NSString stringWithFormat:@"%@banner/3.png", tourGastronomicoWSBaseUrl];
            break;
        case 11:
            stringURL = [NSString stringWithFormat:@"%@banner/4.png", tourGastronomicoWSBaseUrl];
            break;
        case 12:
            stringURL = [NSString stringWithFormat:@"%@banner/4.png", tourGastronomicoWSBaseUrl];
            break;
        default:
            stringURL = [NSString stringWithFormat:@"%@banner/1.png", tourGastronomicoWSBaseUrl];
            break;
    }
    
    bannerURL = [[NSURL alloc] initWithString:stringURL];
    return bannerURL;
}

- (NSURL *) bannerURLforRestaurants{
    NSURL *bannerURL;
    NSString *stringURL;
    
    switch (self.idCity) {
        case 1:
            stringURL = [NSString stringWithFormat:@"%@banner/5.png", tourGastronomicoWSBaseUrl];
            break;
        case 10:
            stringURL = [NSString stringWithFormat:@"%@banner/7.png", tourGastronomicoWSBaseUrl];
            break;
        case 11:
            stringURL = [NSString stringWithFormat:@"%@banner/9.png", tourGastronomicoWSBaseUrl];
            break;
        case 12:
            stringURL = [NSString stringWithFormat:@"%@banner/10.png", tourGastronomicoWSBaseUrl];
            break;
        default:
            stringURL = [NSString stringWithFormat:@"%@banner/1.png", tourGastronomicoWSBaseUrl];
            break;
    }
    
    bannerURL = [[NSURL alloc] initWithString:stringURL];
    return bannerURL;
}
- (NSURL *) bannerURLforHotels{
    NSURL *bannerURL;
    NSString *stringURL;
    
    switch (self.idCity) {
        case 1:
            stringURL = [NSString stringWithFormat:@"%@banner/6.png", tourGastronomicoWSBaseUrl];
            break;
        case 10:
            stringURL = [NSString stringWithFormat:@"%@banner/8.png", tourGastronomicoWSBaseUrl];
            break;
        case 11:
            stringURL = [NSString stringWithFormat:@"%@banner/10.png", tourGastronomicoWSBaseUrl];
            break;
        case 12:
            stringURL = [NSString stringWithFormat:@"%@banner/10.png", tourGastronomicoWSBaseUrl];
            break;
        default:
            stringURL = [NSString stringWithFormat:@"%@banner/1.png", tourGastronomicoWSBaseUrl];
            break;
    }
    
    bannerURL = [[NSURL alloc] initWithString:stringURL];
    return bannerURL;
}

@end
