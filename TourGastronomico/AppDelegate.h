//
//  AppDelegate.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 13/3/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGSCity.h"
#import "TGSSelectCityViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

