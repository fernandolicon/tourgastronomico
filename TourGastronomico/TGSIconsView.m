//
//  TGSIconsView.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 23/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

/*
    For restaurants icons char are as following:
    C - Coin
    B - Card
    A - Car
 */

#import "TGSIconsView.h"

@implementation TGSIconsView

@synthesize iconsString;
@synthesize imageViews;
@synthesize midMargin;
@synthesize leftMargin;
@synthesize minImageSize;

- (void)baseInit {
    iconsString = [[NSString alloc] init];
    imageViews = [[NSMutableArray alloc] init];
    midMargin = 1;
    leftMargin = 0;
    minImageSize = CGSizeMake(5, 5);
}

- (void) assignRestaurant {
    images = @{@"c" : @"Coin.png", @"b" : @"Card.png", @"a" : @"Car.png"};
    isRestaurant = YES;
}

- (void)assignHotel {
    midMargin = 0;
    images = @{@"d" : @"music.png", @"e" : @"computercenter.png", @"f" : @"parking.png",@"g" : @"pool.png", @"h" : @"restaurant.png", @"i" : @"internet.png", @"j" : @"bar.png", @"k" : @"gym.png", @"l" : @"shop.png", @"m" : @"meetingroom.png", @"n" : @"disability.png"};
    isRestaurant = NO;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self baseInit];
    }
    return self;
}

- (void)refresh {
    if (isRestaurant) {
        for(int i = 0; i < self.imageViews.count; ++i) {
            char icon = [self.iconsString characterAtIndex:i];
            UIImageView *imageView = [self.imageViews objectAtIndex:i];
            switch (icon) {
                case 'a':
                    imageView.image = [self setImagewithName:[images objectForKey:@"a"]];
                    break;
                case 'b':
                    imageView.image = [self setImagewithName:[images objectForKey:@"b"]];
                    break;
                case 'c':
                    imageView.image = [self setImagewithName:[images objectForKey:@"c"]];
                    break;
                default:
                    break;
            }
        }
    }else{
        for(int i = 0; i < self.imageViews.count; ++i) {
            char icon = [self.iconsString characterAtIndex:i];
            UIImageView *imageView = [self.imageViews objectAtIndex:i];
            switch (icon) {
                case 'd':
                    imageView.image = [self setImagewithName:[images objectForKey:@"d"]];
                    break;
                case 'e':
                    imageView.image = [self setImagewithName:[images objectForKey:@"e"]];
                    break;
                case 'f':
                    imageView.image = [self setImagewithName:[images objectForKey:@"f"]];
                    break;
                case 'g':
                    imageView.image = [self setImagewithName:[images objectForKey:@"g"]];
                    break;
                case 'h':
                    imageView.image = [self setImagewithName:[images objectForKey:@"h"]];
                    break;
                case 'i':
                    imageView.image = [self setImagewithName:[images objectForKey:@"i"]];
                    break;
                case 'j':
                    imageView.image = [self setImagewithName:[images objectForKey:@"j"]];
                    break;
                case 'k':
                    imageView.image = [self setImagewithName:[images objectForKey:@"k"]];
                    break;
                case 'l':
                    imageView.image = [self setImagewithName:[images objectForKey:@"l"]];
                    break;
                case 'm':
                    imageView.image = [self setImagewithName:[images objectForKey:@"m"]];
                    break;
                case 'n':
                    imageView.image = [self setImagewithName:[images objectForKey:@"n"]];
                    break;
                default:
                    break;
            }
        }
    }
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (isRestaurant) {
        float imageWidth = 13;
        float imageHeight = 13;
        
        for (int i = 0; i < self.imageViews.count; ++i) {
            
            UIImageView *imageView = [self.imageViews objectAtIndex:i];
            CGRect imageFrame = CGRectMake(self.leftMargin + i*(self.midMargin+imageWidth), 0, imageWidth, imageHeight);
            imageView.frame = imageFrame;
            
        }
    }else{
        float desiredImageWidth = (self.frame.size.width - (self.leftMargin*2) - (self.midMargin*self.imageViews.count)) / self.imageViews.count;
        float imageWidth = MAX(self.minImageSize.width, desiredImageWidth);
        float imageHeight = 20;
        
        for (int i = 0; i < self.imageViews.count; ++i) {
            
            UIImageView *imageView = [self.imageViews objectAtIndex:i];
            CGRect imageFrame = CGRectMake(self.leftMargin + i*(self.midMargin+imageWidth), 0, imageWidth, imageHeight);
            imageView.frame = imageFrame;
            
        }
    }
    
}

- (void)setIconsfromString: (NSString *) iconsPlace{
    
    self.iconsString = [iconsPlace stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    
    // Remove old image views
    for(int i = 0; i < self.imageViews.count; ++i) {
        UIImageView *imageView = (UIImageView *) [self.imageViews objectAtIndex:i];
        [imageView removeFromSuperview];
    }
    [self.imageViews removeAllObjects];
    
    // Add new image views
    for(int i = 0; i < self.iconsString.length ; ++i) {
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.imageViews addObject:imageView];
        [self addSubview:imageView];
    }
    
    // Relayout and refresh
    [self setNeedsLayout];
    [self refresh];
}

- (UIImage *)setImagewithName:(NSString *)name {
    UIImage *image = [UIImage imageNamed:name];
    return image;
}

@end
