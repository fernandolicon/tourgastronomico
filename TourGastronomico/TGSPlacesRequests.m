//
//  TGSPlacesRequests.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 9/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "TGSPlacesRequests.h"

@implementation TGSPlacesRequests

+ (void) listPlacesforCityID: (NSInteger) cityID ofKind: (int) category forView: (TGSListPlacesViewController *) viewController{
    NSMutableArray *placesFounded = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@getEstablecimientosDeCiudad_json.php?id=%i&categoria=%i", tourGastronomicoWSBaseUrl, (int) cityID, category];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/json",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        for (NSDictionary *location in responseObject) {
            TGSPlace *temporaryPlace = [[TGSPlace alloc] initWithDictionary:location];
            [placesFounded addObject:temporaryPlace];
        }
        [viewController successfulListOfPlaces:placesFounded];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [viewController errorInConnection];
    }];
    
}

+ (void) infoForPlace: (TGSPlace *) place forView: (TGSRestaurantDetailViewController *) viewController{
    NSString *url = [NSString stringWithFormat:@"%@getEstablecimiento_json.php?id=%i", tourGastronomicoWSBaseUrl, (int) place.idPlace];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/json",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSArray *responseObject) {
        NSDictionary *infoDictionary = responseObject[0];
        [place populateBasicInfofwithDictionary:infoDictionary];
        [self detailsForPlace:place forView:viewController];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [viewController errorInConnection];
    }];
}

+ (void) detailsForPlace: (TGSPlace *) place forView: (TGSRestaurantDetailViewController *) viewController{
    NSString *url = [NSString stringWithFormat:@"%@getDetalles_json.php?id=%i", tourGastronomicoWSBaseUrl, (int) place.idPlace];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/json",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSArray *responseObject) {
        if (responseObject.count == 0) {
            place.hasDetails = NO;
            [self picturesForPlace:place forView:viewController];
        }else{
            NSDictionary *infoDictionary = responseObject[0];
            [place populateDetailswithDictionary:infoDictionary];
            [self mapForPlace:place forView:viewController];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [viewController errorInConnection];
    }];
    
}

+ (void) mapForPlace: (TGSPlace *) place forView: (TGSRestaurantDetailViewController *) viewController{
    NSString *googleMaps = [NSString stringWithFormat:@"%@", place.googleMapsLocation];
    if ([googleMaps isEqualToString:@""]) {
        [self picturesForPlace:place forView:viewController];
    }else{
        NSString *url = [NSString stringWithFormat:@"https://www.googleapis.com/urlshortener/v1/url?shortUrl=%@&key=%@", place.googleMapsLocation, googleMapsAPIKey];
        NSURL *requestURL = [NSURL URLWithString:url];
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:requestURL completionHandler:
          ^(NSData *data, NSURLResponse *response, NSError *error) {
              if (!error) {
                  if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                      NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
                      if (statusCode != 200) {
                          NSLog(@"dataTaskWithRequest HTTP status code: %ld", (long)statusCode);
                          return;
                      }
                  }
                  NSError *jsonParseError = nil;
                  NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
                  if (!jsonParseError) {
                      NSString *longUrl = [json objectForKey:@"longUrl"];
                      NSString *pattern = @".*?@([0-9.\\-]*),([0-9.\\-]*),([0-9.\\-]*).*";
                      NSError *regexError = nil;
                      NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&regexError];
                      if (!regexError) {
                          NSArray* matches = [regex matchesInString:longUrl options:0 range:NSMakeRange(0, [longUrl length])];
                          NSString *placeName = [longUrl substringWithRange:[[matches objectAtIndex:0] rangeAtIndex:0]];
                          NSString *latitude = [longUrl substringWithRange:[[matches objectAtIndex:0] rangeAtIndex:1]];
                          NSString *longitude = [longUrl substringWithRange:[[matches objectAtIndex:0] rangeAtIndex:2]];
                          NSString *zoom = [longUrl substringWithRange:[[matches objectAtIndex:0] rangeAtIndex:3]];
                          NSString *openURL = [NSString stringWithFormat:@"comgooglemaps://?q=%@&center=%@,%@&zoom=%@", placeName, latitude, longitude, zoom];
                          place.googleMapsURL = [NSURL URLWithString:openURL];
                          [self picturesForPlace:place forView:viewController];
                      } else {
                          NSLog(@"REGEX error: %@", regexError);
                      }
                  } else {
                      NSLog(@"JSON parse error: %@", jsonParseError);
                  }
              } else {
                  NSLog(@"API request error: %@", error);
              }
          }] resume];
    }
}

+ (void) picturesForPlace: (TGSPlace *) place forView: (TGSRestaurantDetailViewController *) viewController{
    NSString *url = [NSString stringWithFormat:@"%@getFotos_json.php?id=%i", tourGastronomicoWSBaseUrl, (int) place.idPlace];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/json",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSArray *responseObject) {
        place.imageURLs = [[NSMutableArray alloc] init];
        for (NSDictionary *imageDictionary in responseObject) {
            NSString *image = [imageDictionary objectForKey:@"url"];
            NSString *imageURLString = [NSString stringWithFormat:@"%@images/%@.png", tourGastronomicoWSBaseUrl, image];
            [place.imageURLs addObject: imageURLString];
        }
        [viewController successfulPlace:place];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [viewController errorInConnection];
    }];
}

+ (void) reviewForPlaceWithID: (int) placeID withGrade: (long) grade forView: (TGSRestaurantDetailViewController *) viewController{
    NSString *url = [NSString stringWithFormat:@"%@votar.php?calificacion=%ld&id=%i", tourGastronomicoWSBaseUrl, grade, placeID];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/json",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSArray *responseObject) {
        [viewController successfulReview];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [viewController errorInConnectionforReview];
    }];
}

+ (void) infoForHotel: (TGSPlace *) place forView: (TGSHotelDetailViewController *) viewController{
    NSString *url = [NSString stringWithFormat:@"%@getEstablecimiento_json.php?id=%i", tourGastronomicoWSBaseUrl, (int) place.idPlace];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/json",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSArray *responseObject) {
        NSDictionary *infoDictionary = responseObject[0];
        [place populateBasicInfofwithDictionary:infoDictionary];
        [self detailsForHotel:place forView:viewController];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [viewController errorInConnection];
    }];
}

+ (void) detailsForHotel: (TGSPlace *) place forView: (TGSHotelDetailViewController *) viewController{
    NSString *url = [NSString stringWithFormat:@"%@getDetalles_json.php?id=%i", tourGastronomicoWSBaseUrl, (int) place.idPlace];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/json",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSArray *responseObject) {
        if (responseObject.count == 0) {
            place.hasDetails = NO;
            [self picturesForHotel:place forView:viewController];
        }else{
            NSDictionary *infoDictionary = responseObject[0];
            [place populateDetailswithDictionaryforHotel:infoDictionary];
            [self mapForHotel:place forView:viewController];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [viewController errorInConnection];
    }];
}

+ (void) mapForHotel: (TGSPlace *) place forView: (TGSHotelDetailViewController *) viewController{
    NSString *googleMaps = [NSString stringWithFormat:@"%@", place.googleMapsLocation];
    if ([googleMaps isEqualToString:@""]) {
        [self picturesForHotel:place forView:viewController];
    }else{
        NSString *url = [NSString stringWithFormat:@"https://www.googleapis.com/urlshortener/v1/url?shortUrl=%@&key=%@", place.googleMapsLocation, googleMapsAPIKey];
        NSURL *requestURL = [NSURL URLWithString:url];
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:requestURL completionHandler:
          ^(NSData *data, NSURLResponse *response, NSError *error) {
              if (!error) {
                  if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                      NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
                      if (statusCode != 200) {
                          NSLog(@"dataTaskWithRequest HTTP status code: %ld", (long)statusCode);
                          return;
                      }
                  }
                  NSError *jsonParseError = nil;
                  NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];
                  if (!jsonParseError) {
                      NSString *longUrl = [json objectForKey:@"longUrl"];
                      NSString *pattern = @".*?@([0-9.\\-]*),([0-9.\\-]*),([0-9.\\-]*).*";
                      NSError *regexError = nil;
                      NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&regexError];
                      if (!regexError) {
                          NSArray* matches = [regex matchesInString:longUrl options:0 range:NSMakeRange(0, [longUrl length])];
                          NSString *placeName = [longUrl substringWithRange:[[matches objectAtIndex:0] rangeAtIndex:0]];
                          NSString *latitude = [longUrl substringWithRange:[[matches objectAtIndex:0] rangeAtIndex:1]];
                          NSString *longitude = [longUrl substringWithRange:[[matches objectAtIndex:0] rangeAtIndex:2]];
                          NSString *zoom = [longUrl substringWithRange:[[matches objectAtIndex:0] rangeAtIndex:3]];
                          NSString *openURL = [NSString stringWithFormat:@"comgooglemaps://?q=%@&center=%@,%@&zoom=%@", placeName, latitude, longitude, zoom];
                          place.googleMapsURL = [NSURL URLWithString:openURL];
                          [self picturesForHotel:place forView:viewController];
                      } else {
                          NSLog(@"REGEX error: %@", regexError);
                      }
                  } else {
                      NSLog(@"JSON parse error: %@", jsonParseError);
                  }
              } else {
                  NSLog(@"API request error: %@", error);
              }
          }] resume];
    }
}

+ (void) picturesForHotel: (TGSPlace *) place forView: (TGSHotelDetailViewController *) viewController{
    NSString *url = [NSString stringWithFormat:@"%@getFotos_json.php?id=%i", tourGastronomicoWSBaseUrl, (int) place.idPlace];
    NSURL *requestURL = [NSURL URLWithString:url];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:requestURL];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/json",nil];
    
    [manager GET:[manager.baseURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, NSArray *responseObject) {
        place.imageURLs = [[NSMutableArray alloc] init];
        for (NSDictionary *imageDictionary in responseObject) {
            NSString *image = [imageDictionary objectForKey:@"url"];
            NSString *imageURLString = [NSString stringWithFormat:@"%@images/%@.png", tourGastronomicoWSBaseUrl, image];
            [place.imageURLs addObject: imageURLString];
        }
        [viewController successfulPlace:place];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [viewController errorInConnection];
    }];
}

@end
