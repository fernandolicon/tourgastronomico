//
//  LFMRateView.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 18/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LFMRateView;

@protocol RateViewDelegate
- (void)rateView:(LFMRateView *)rateView ratingDidChange:(float)rating;
@end

@interface LFMRateView : UIView{
    NSString *text;
}

@property (strong, nonatomic) UIImage *notSelectedImage;
@property (strong, nonatomic) UIImage *halfSelectedImage;
@property (strong, nonatomic) UIImage *fullSelectedImage;
@property (assign, nonatomic) float rating;
@property (assign) BOOL editable;
@property (strong) NSMutableArray * imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (assign) id <RateViewDelegate> delegate;

- (void) assignRestaurant;
- (void)assignHotel;

@end
