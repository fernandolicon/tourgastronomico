//
//  TGSListPlacesViewController.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 8/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import "TGSCity.h"
#import "TGSPlace.h"
#import "TGSPlacesRequests.h"
#import "TGSRestaurantDetailViewController.h"
#import "TGSHotelDetailViewController.h"
#import "TGSPlaceCell.h"

@interface TGSListPlacesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    __weak IBOutlet UITableView *placesTbl;
    NSArray *listPlaces;
    BOOL dataFetched;
    BOOL noDataFound;
    TGSPlace *selectedPlace;
    __weak IBOutlet UIButton *returnBttn;
    __weak IBOutlet UIImageView *bannerImage;
}

@property (nonatomic, strong) TGSCity *selectedCity;
@property int selection;

- (IBAction)returnToCity:(id)sender;
- (IBAction)placeDetail:(id)sender;
- (void) successfulListOfPlaces: (NSMutableArray *) places;
- (void) errorInConnection;

@end
