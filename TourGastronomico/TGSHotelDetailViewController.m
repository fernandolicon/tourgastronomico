//
//  TGSHotelDetailViewController.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 27/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "TGSHotelDetailViewController.h"

@interface TGSHotelDetailViewController ()

@end

@implementation TGSHotelDetailViewController

@synthesize selectedPlace;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    hotelName.text = selectedPlace.name;
    
    [infoImages assignHotel];
    [hotelStars assignHotel];
    hotelStars.delegate = self;
    
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:75/255.0f green:75/255.0f blue:75/255.0f alpha:1.0f]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD show];
    [TGSPlacesRequests infoForHotel:selectedPlace forView:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rate view delegate

- (void)rateView:(LFMRateView *)rateView ratingDidChange:(float)rating{
    
}

#pragma mark - Button actions

- (IBAction)googleMaps:(id)sender {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:selectedPlace.googleMapsURL];
    } else {
        [[UIApplication sharedApplication] openURL:selectedPlace.googleMapsLocation];
    }
}

- (IBAction)returnToView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Network responses

- (void) successfulPlace: (TGSPlace *) place{
    [SVProgressHUD dismiss];
    selectedPlace = place;
    [self populateViewWithInfo];
}

- (void) errorInConnection{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Hubo un problema" message:@"Hubo un error al contactar al servidor, intentelo más tarde por favor." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) populateViewWithInfo{
    NSString *mapsUrl = [NSString stringWithFormat:@"%@", selectedPlace.googleMapsLocation];
    if (!([mapsUrl isEqualToString:@""])) {
        [googleMapsBttn setHidden:NO];
    }
    hotelStars.rating = selectedPlace.rating;
    [infoImages setIconsfromString:selectedPlace.infoDetail];
    addressLbl.text = selectedPlace.address;
    contactLbl.text = selectedPlace.phoneNumber;
    
    if (selectedPlace.imageURLs.count >= 1) {
        NSURL *hotelImageUrl = [NSURL URLWithString:selectedPlace.imageURLs[0]];
        [hotelImage hnk_setImageFromURL:hotelImageUrl];
    }
}

@end
