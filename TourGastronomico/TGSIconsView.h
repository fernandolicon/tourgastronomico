//
//  TGSIconsView.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 23/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//
//  Class developed using as basis LFMRateView (someone else ownership, but I don't know who).
//

#import <UIKit/UIKit.h>

@interface TGSIconsView : UIView{
    NSDictionary *images;
    BOOL isRestaurant;
}

@property (strong, nonatomic) NSString *iconsString;
@property (strong) NSMutableArray * imageViews;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;


- (void) assignRestaurant;
- (void)assignHotel;
- (void)setIconsfromString: (NSString *) iconsPlace;

@end
