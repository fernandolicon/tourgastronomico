//
//  TSGSelectCityViewController.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 6/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "TGSSelectCityViewController.h"

@interface TGSSelectCityViewController ()

@end

@implementation TGSSelectCityViewController{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

@synthesize isRootController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    
    chihuahuaBttn.layer.cornerRadius = 10.0;
    juarezBttn.layer.cornerRadius = 10.0;
    cuauhtemocBttn.layer.cornerRadius = 10.0;
    deliciasBttn.layer.cornerRadius = 10.0;
    
    if (isRootController) {
        locationManager = [[CLLocationManager alloc] init];
        geocoder = [[CLGeocoder alloc] init];
        
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        locationManager.delegate = self;
        
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
        
        [locationManager startUpdatingLocation];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)selectCity:(id)sender {
    TGSCity *buttonCity = [[TGSCity alloc] init];
    buttonCity.idCity = [sender tag];
    
    switch ((long) buttonCity.idCity) {
        case 1:
            buttonCity.name = @"CHIHUAHUA";
            break;
        case 10:
            buttonCity.name = @"CIUDAD JUÁREZ";
            break;
        case 11:
            buttonCity.name = @"CUAUHTÉMOC";
            break;
        case 12:
            buttonCity.name = @"DELICIAS";
            break;
        default:
            buttonCity.name = @"No City";
            break;
    }
    [self userSelectedCity: buttonCity];
}

- (void) userSelectedCity:(TGSCity *) userCity{
    NSString *idCity = [NSString stringWithFormat:@"%ld", (long) userCity.idCity];
    
    [[NSUserDefaults standardUserDefaults] setObject:userCity.name forKey:@"cityName"];
    [[NSUserDefaults standardUserDefaults] setObject:idCity forKey:@"idCity"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (!isRootController){
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        isRootController = false;
        [self performSegueWithIdentifier:@"chosenCity" sender:nil];
    }
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No se pudo obtener ciudad" message:@"Hubo un error al obtener la ciudad. Selecciona una ciudad de la lista, por favor." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = [locations lastObject];
    
    // Reverse Geocoding
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            NSString *userCity = [placemark.locality uppercaseString];
            if ([self isCityOnListforLocation:userCity]){
                if ([userCity isEqualToString:@"CIUDAD CUAUHTEMOC"]) {
                    userCity = @"CUAUHTÉMOC";
                }
                TGSCity *selectedCity = [self cityForLocation:userCity];
                [self userSelectedCity:selectedCity];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No se pudo obtener ciudad" message:@"Tu ciudad no esta disponible, selecciona una ciudad de la lista, por favor." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No se pudo obtener ciudad" message:@"Hubo un error al obtener la ciudad. Selecciona una ciudad, por favor." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } ];
    [locationManager stopUpdatingLocation];
}

- (BOOL)isCityOnListforLocation: (NSString *) location{
    if ([location isEqualToString:@"CHIHUAHUA"] || [location isEqualToString:@"CIUDAD JUÁREZ"] || [location isEqualToString:@"CIUDAD CUAUHTEMOC"] || [location isEqualToString:@"DELICIAS"]) {
        return true;
    }
    
    return false;
}

- (TGSCity *)cityForLocation: (NSString *) location{
    TGSCity *city = [[TGSCity alloc] init];
    
    if ([location isEqualToString:@"CHIHUAHUA"]){
        city.name = @"CHIHUAHUA";
        city.idCity = 1;
        return city;
    }
    if ([location isEqualToString:@"CIUDAD JUÁREZ"]){
        city.name = @"CIUDAD JUÁREZ";
        city.idCity = 10;
        return city;
    }
    if ([location isEqualToString:@"CUAUHTÉMOC"]) {
        city.name = @"CUAUHTÉMOC";
        city.idCity = 11;
        return city;
    }
    if ([location isEqualToString:@"DELICIAS"]) {
        city.name = @"DELICIAS";
        city.idCity = 12;
        return city;
    }
    
    return city;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

 #pragma mark - Navigation
/*
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if ([segue.identifier isEqualToString:@"cityChosen"]) {
         TGSCityView *vc = [segue destinationViewController];
     }
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }*/

@end
