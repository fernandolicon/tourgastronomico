//
//  TGSListPlacesViewController.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 8/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "TGSListPlacesViewController.h"
#import "Haneke.h"

@interface TGSListPlacesViewController ()

@end

@implementation TGSListPlacesViewController

@synthesize selectedCity;
@synthesize selection;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    listPlaces = [[NSArray alloc] init];
    
    dataFetched = NO;
    noDataFound = NO;
    
    selectedPlace = [[TGSPlace alloc] init];
    
    [TGSPlacesRequests listPlacesforCityID:selectedCity.idCity ofKind:selection forView:self];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:75/255.0f green:75/255.0f blue:75/255.0f alpha:1.0f]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD show];
    [self setImageBanner];
    
    placesTbl.backgroundColor = [UIColor colorWithRed:75/255.0f green:75/255.0f blue:75/255.0f alpha:0.0f];
}

- (void) viewWillDisappear:(BOOL)animated{
    placesTbl = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setImageBanner{
    NSURL *bannerCity = [[NSURL alloc] init];
    if (selection == 1) {
        bannerCity = [selectedCity bannerURLforRestaurants];
    }else{
        bannerCity = [selectedCity bannerURLforHotels];
    }
    [bannerImage hnk_setImageFromURL:bannerCity];
}

#pragma mark - TableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (!(dataFetched && listPlaces.count == 0)) {
        return listPlaces.count;
    }else{
        return 1;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (listPlaces.count != 0) {
        TGSPlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"placecell" forIndexPath:indexPath];
        TGSPlace *provisionalPlace = listPlaces[indexPath.row];
        
        [cell.placeButton setTitle:provisionalPlace.name forState:UIControlStateNormal];
        cell.placeButton.tag = provisionalPlace.idPlace;
        
        return cell;
    }else{
        TGSPlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"placecell" forIndexPath:indexPath];
        
        cell.placeButton.tag = -1;
        [cell.placeButton setTitle:@"No hay lugares\ndisponibles." forState:UIControlStateNormal];
        cell.placeButton.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        cell.placeButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        return cell;
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetails"]) {
        TGSRestaurantDetailViewController *vc = [segue destinationViewController];
        vc.selectedPlace = selectedPlace;
    }
    //showHotelDetails
    if ([segue.identifier isEqualToString:@"showHotelDetails"]) {
        TGSHotelDetailViewController *vc = [segue destinationViewController];
        vc.selectedPlace = selectedPlace;
    }
}

- (IBAction)returnToCity:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)placeDetail:(id)sender {
    UIButton *selectedButton = (UIButton *) sender;
    if (selectedButton.tag != -1) {
        selectedPlace.name = selectedButton.titleLabel.text;
        selectedPlace.idPlace = (int) [sender tag];
        if (selection == 1) {
            [self performSegueWithIdentifier:@"showDetails" sender:nil];
        }else{
            [self performSegueWithIdentifier:@"showHotelDetails" sender:nil];
        }
    }
}

#pragma mark - Network Responses

- (void) successfulListOfPlaces: (NSMutableArray *) places{
    listPlaces =  places;
    dataFetched = YES;
    [placesTbl performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    [SVProgressHUD dismiss];
}

- (void) errorInConnection{
    [SVProgressHUD dismiss];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Hubo un problema" message:@"Hubo un error al contactar al servidor, intentelo más tarde por favor." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
