//
//  TGSPlacesRequests.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 9/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "TGSPlace.h"
#import "TGSListPlacesViewController.h"
#import "TGSRestaurantDetailViewController.h"
#import "TGSHotelDetailViewController.h"
#import "Settings.h"
@class TGSListPlacesViewController;
@class TGSRestaurantDetailViewController;
@class TGSHotelDetailViewController;

@interface TGSPlacesRequests : NSObject

+ (void) listPlacesforCityID: (NSInteger) cityID ofKind: (int) category forView: (TGSListPlacesViewController *) viewController;
+ (void) infoForPlace: (TGSPlace *) place forView: (TGSRestaurantDetailViewController *) viewController;
+ (void) detailsForPlace: (TGSPlace *) place forView: (TGSRestaurantDetailViewController *) viewController;
+ (void) picturesForPlace: (TGSPlace *) place forView: (TGSRestaurantDetailViewController *) viewController;
+ (void) reviewForPlaceWithID: (int) placeID withGrade: (long) grade forView: (TGSRestaurantDetailViewController *) viewController;
+ (void) infoForHotel: (TGSPlace *) place forView: (TGSHotelDetailViewController *) viewController;
+ (void) detailsForHotel: (TGSPlace *) place forView: (TGSHotelDetailViewController *) viewController;
+ (void) picturesForHotel: (TGSPlace *) place forView: (TGSHotelDetailViewController *) viewController;

@end
