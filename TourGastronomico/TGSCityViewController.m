//
//  ViewController.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 13/3/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "MapKit/MapKit.h"
#import "TGSCityViewController.h"
#import "TGSListPlacesViewController.h"
#import "Haneke.h"


@interface TGSCityViewController ()

@end

@implementation TGSCityViewController;

@synthesize citySelected;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setHidden:YES];
    
    [self.navigationController.navigationBar setHidden:YES];
    
    citySelected = [TGSCity userPreferencesCity];
    [self populateViewWithCity];
        
    self.view.backgroundColor = [UIColor colorWithRed:14/255.0f green:14/255.0f blue:14/255.0f alpha:1.0f];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    UIFont *customFont = [UIFont fontWithName:@"PFRegalDisplayPro-Bold" size:30];
    
    cityLabel.font = customFont;
    
    restaurantButton.layer.cornerRadius = 10.0;
    hotelsButtons.layer.cornerRadius = 10.0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNotification:)
                                                 name:SVProgressHUDWillDisappearNotification
                                               object:nil];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.view setHidden:YES];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:75/255.0f green:75/255.0f blue:75/255.0f alpha:1.0f]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD show];
    citySelected = [TGSCity userPreferencesCity];
    [self populateViewWithCity];
    [self setImageBanner];
}

- (void)handleNotification:(NSNotification *)notif
{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Cities methods

- (void) populateViewWithCity{
    cityLabel.text = [NSString stringWithFormat:@"·%@·", citySelected.name];
    [SVProgressHUD dismiss];
    [self.view setHidden:NO];
}

- (void) setImageBanner{
    NSURL *bannerCity = [citySelected bannerURL];
    [bannerImage hnk_setImageFromURL:bannerCity];
}

- (void) noLocationAvailable{
        [SVProgressHUD dismiss];
        [self performSegueWithIdentifier:@"chooseCity" sender:nil];
}

- (IBAction)listPlaces:(id)sender {
    [self performSegueWithIdentifier:@"listPlaces" sender:sender];
}

- (IBAction)chooseCity:(id)sender {
    [self performSegueWithIdentifier:@"chooseCity" sender:nil];
}


 #pragma mark - Navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     
     if ([segue.identifier isEqualToString:@"listPlaces"]) {
         TGSListPlacesViewController *nextVC = segue.destinationViewController;
         nextVC.selectedCity = citySelected;
         nextVC.selection = (int) [sender tag];
     }
 }

@end
