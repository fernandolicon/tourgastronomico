//
//  TSGPlace.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 16/3/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "TGSPlace.h"

@implementation TGSPlace

- (instancetype) initWithDictionary: (NSDictionary *) responseObject{
    if (self = [super init]){
        self.name = [responseObject objectForKey:@"nombre"];
        self.idPlace = [[responseObject objectForKey:@"id"] intValue];
    }
    
    return self;
}

- (void) populateBasicInfofwithDictionary: (NSDictionary *) responseObject{
    self.address = [responseObject objectForKey:@"direccion"];
    self.phoneNumber = [responseObject objectForKey:@"telefono"];
    self.schedule = [responseObject objectForKey:@"horario"];
}

- (void) populateDetailswithDictionary: (NSDictionary *) responseObject{
    NSString *googleMapsUrl = [responseObject objectForKey:@"mapa"];
    self.googleMapsLocation = [NSURL URLWithString:googleMapsUrl];
    NSString *suggestionsString = [responseObject objectForKey:@"recomendaciones"];
    suggestionsString = [suggestionsString stringByReplacingOccurrencesOfString:@"&" withString:@"\r &"];
    DBGHTMLEntityDecoder *decoder = [[DBGHTMLEntityDecoder alloc] init];
    self.suggestions = [decoder decodeString:suggestionsString];
    self.descriptionLocation = [responseObject objectForKey:@"descripcion"];
    self.engDescription = [responseObject objectForKey:@"descripcion_ing"];
    self.infoDetail = [responseObject objectForKey:@"tags"];
    self.hasDetails = YES;
    
    //Claculate rounded number to reliability with Android App
    NSString *gradeString = [responseObject objectForKeyedSubscript:@"calificacion"];
    NSString *votesString = [responseObject objectForKeyedSubscript:@"votos"];
    NSInteger grade = [gradeString integerValue];
    NSInteger votes = [votesString integerValue];
    if (votes != 0) {
        float ratingBefore = grade/votes;
        int roundedDown = ratingBefore;
        float modular = ratingBefore - roundedDown;
        if (modular > 0.5) {
            self.rating = roundedDown + 1;
        }else{
            self.rating = roundedDown;
        }
    }else{
        self.rating = 0;
    }

}

- (void) populateDetailswithDictionaryforHotel: (NSDictionary *) responseObject{
    NSString *googleMapsUrl = [responseObject objectForKey:@"mapa"];
    self.googleMapsLocation = [NSURL URLWithString:googleMapsUrl];
    self.infoDetail = [responseObject objectForKey:@"tags"];
    self.rating = (int)[responseObject objectForKeyedSubscript:@"calificacion"];
}

- (NSString *) descriptionInfo{
    NSString *description = [NSString stringWithFormat:@"Name: %@, Address: %@, Phone: %@, Schedule: %@", self.name, self.address, self.phoneNumber, self.schedule];
    
    return description;
}

- (NSString *) descriptionDetail{
    NSString *description = [NSString stringWithFormat:@"GoogleMapsUrl: %@, Suggestions: %@, Description: %@, Description English: %@, Icons: %@, Rating: %ld", self.googleMapsLocation, self.suggestions, self.descriptionLocation, self.engDescription, self.infoDetail, (long) self.rating];
    
    return description;
}

@end
