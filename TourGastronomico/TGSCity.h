//
//  TGSCity.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 16/3/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Settings.h"

@interface TGSCity : NSObject

@property (nonatomic, copy) NSString *name;
@property NSInteger idCity;

- (instancetype) initWithDictionary:(NSDictionary *) cityDictionary;

+ (TGSCity *) userPreferencesCity;
- (NSURL *) bannerURL;
- (NSURL *) bannerURLforRestaurants;
- (NSURL *) bannerURLforHotels;

@end
