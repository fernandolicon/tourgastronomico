//
//  TGSImageFullScreenViewController.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 23/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KIImagePager.h"

@interface TGSImageFullScreenViewController : UIViewController <KIImagePagerDataSource, KIImagePagerDelegate>{
    
    __weak IBOutlet KIImagePager *imagePager;
}

@property (strong, nonatomic) NSArray *imageURLs;
@property NSInteger imagePage;

- (IBAction)returnToView:(id)sender;
@end
