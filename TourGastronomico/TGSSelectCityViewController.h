//
//  TSGSelectCityViewController.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 6/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "TGSCity.h"
#import "TGSCityViewController.h"

@interface TGSSelectCityViewController : UIViewController<CLLocationManagerDelegate>{
    
    __weak IBOutlet UIButton *juarezBttn;
    __weak IBOutlet UIButton *chihuahuaBttn;
    __weak IBOutlet UIButton *cuauhtemocBttn;
    __weak IBOutlet UIButton *deliciasBttn;
}

@property BOOL isRootController;


- (IBAction)selectCity:(id)sender;

@end
