//
//  RestaurantDetailViewController.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 17/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import "TGSCity.h"
#import "TGSPlace.h"
#import "TGSPlacesRequests.h"
#import "LFMRateView.h"
#import "KIImagePager.h"
#import "TGSIconsView.h"
#import "TGSImageFullScreenViewController.h"
#import "TGSDetailPlaceViewController.h"

@interface TGSRestaurantDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RateViewDelegate, KIImagePagerDataSource, KIImagePagerDelegate>{
    
    __weak IBOutlet UILabel *restaurantName;
    __weak IBOutlet LFMRateView *starsView;
    bool allConnectionsPerformed;
    bool hasSuggestions;
    bool hasDescription;
    bool isFirstTime;
    NSUInteger page;
    __weak IBOutlet UILabel *addressLbl;
    __weak IBOutlet UILabel *scheduleLbl;
    __weak IBOutlet UILabel *phoneLbl;
    __weak IBOutlet KIImagePager *imagesView;
    __weak IBOutlet UIButton *googleBttn;
    __weak IBOutlet UITableView *detailTbl;
    __weak IBOutlet UIButton *ratePlace;
    __weak IBOutlet TGSIconsView *iconsView;
    __weak IBOutlet UIButton *returnBttn;
    UIApplication *networkIdentifier;
}

@property (nonatomic, strong) TGSPlace *selectedPlace;

- (IBAction)returnToView:(id)sender;
- (IBAction)googleMaps:(id)sender;
- (IBAction)seeDetail:(id)sender;
- (void) successfulPlace: (TGSPlace *) place;
- (void) successfulReview;
- (void) errorInConnection;
- (void) errorInConnectionforReview;


@end
