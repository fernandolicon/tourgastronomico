//
//  RestaurantDetailViewController.m
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 17/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import "TGSRestaurantDetailViewController.h"

@interface TGSRestaurantDetailViewController ()

@end

@implementation TGSRestaurantDetailViewController

@synthesize selectedPlace;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    restaurantName.text = selectedPlace.name;
    
    allConnectionsPerformed = NO;
    hasDescription = NO;
    hasSuggestions = NO;
    isFirstTime = NO;
    
    [iconsView assignRestaurant];
    [starsView assignRestaurant];
    [googleBttn setHidden:YES];
    starsView.delegate = self;
    [starsView setEditable:YES];
    
    imagesView.pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    imagesView.slideshowTimeInterval = 5.0f;
    [imagesView setImageCounterDisabled:YES];
    
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:75/255.0f green:75/255.0f blue:75/255.0f alpha:1.0f]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD show];
    [TGSPlacesRequests infoForPlace:selectedPlace forView:self];
    
    networkIdentifier = [UIApplication sharedApplication];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)rateView:(LFMRateView *)rateView ratingDidChange:(float)rating{
    networkIdentifier.networkActivityIndicatorVisible = YES;
    [TGSPlacesRequests reviewForPlaceWithID:selectedPlace.idPlace withGrade:rateView.rating forView:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    detailTbl = nil;
}

#pragma mark - TableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (!allConnectionsPerformed) {
        return 0;
    }else if (!selectedPlace.hasDetails) {
        return 2;
    }else {
        return 2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!selectedPlace.hasDetails) {
        if (indexPath.row == 0) {
            TGSPlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];
            [cell.placeButton setTitle:@"" forState:UIControlStateNormal];
            return cell;
        }else{
            TGSPlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];
            cell.placeButton.tag = -1;
            [cell.placeButton setTitle:@"No hay detalles\ndisponibles." forState:UIControlStateNormal];
            cell.placeButton.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
            cell.placeButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            return cell;
        }
    }else{
        if (indexPath.row == 0) {
            if (hasSuggestions) {
                TGSPlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];
                [cell.placeButton setTitle:@"RECOMENDACIONES" forState:UIControlStateNormal];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.placeButton.tag = 1;
                return cell;
            }else if (hasDescription) {
                TGSPlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];
                [cell.placeButton setTitle:@"DESCRIPCIÓN" forState:UIControlStateNormal];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.placeButton.tag = 2;
                return cell;
            }else{
                TGSPlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];
                [cell.placeButton setTitle:@"" forState:UIControlStateNormal];
                return cell;
            }
        }else{
            if (hasDescription) {
                TGSPlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];
                [cell.placeButton setTitle:@"DESCRIPCIÓN" forState:UIControlStateNormal];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.placeButton.tag = 2;
                return cell;
            }else{
                TGSPlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];
                [cell.placeButton setTitle:@"" forState:UIControlStateNormal];
                return cell;
            }
        }
    }
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor colorWithRed:14/255.0f green:14/255.0f blue:14/255.0f alpha:1.0f];
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"seeImages"]) {
        TGSImageFullScreenViewController *vc = [segue destinationViewController];
        vc.imagePage = page;
        vc.imageURLs = selectedPlace.imageURLs;
    }
    
    if ([segue.identifier isEqualToString:@"seeDetail"]) {
        TGSDetailPlaceViewController *vc = [segue destinationViewController];
        int tag = (int) [sender tag];
        if (tag == 1) {
            vc.detail = selectedPlace.suggestions;
        }else{
            vc.detail = [NSString stringWithFormat:@"%@\r\r%@", selectedPlace.descriptionLocation, selectedPlace.engDescription];
        }
    }
}


- (IBAction)returnToView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)googleMaps:(id)sender {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:selectedPlace.googleMapsURL];
    } else {
        [[UIApplication sharedApplication] openURL:selectedPlace.googleMapsLocation];
    }
}

- (IBAction)seeDetail:(id)sender {
    UIButton *selectedButton = (UIButton *) sender;
    if (selectedButton.tag != -1) {
        [self performSegueWithIdentifier:@"seeDetail" sender:sender];
    }
}

#pragma mark - Image Pager Methods

- (NSArray *) arrayWithImages:(KIImagePager *)pager{
    if (allConnectionsPerformed) {
        return selectedPlace.imageURLs;
    }else{
        return nil;
    }
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager*)pager
{
    return UIViewContentModeScaleAspectFill;
}

- (void) imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
    page = index;
    [self performSegueWithIdentifier:@"seeImages" sender:nil];
}

#pragma mark - Network Responses

- (void) successfulPlace: (TGSPlace *) place{
    allConnectionsPerformed = YES;
    [SVProgressHUD dismiss];
    selectedPlace = place;
    [self populateViewWithInfo];
}

- (void) successfulReview{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Tu voto fue registrado" message:@"Tu voto ha sido registrado. ¡Gracias!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) errorInConnection{
    [SVProgressHUD dismiss];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Hubo un problema" message:@"Hubo un error al contactar al servidor, intentelo más tarde por favor." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) errorInConnectionforReview{
    networkIdentifier.networkActivityIndicatorVisible = NO;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Hubo un problema" message:@"Hubo un error al guardar la calificación, intentelo más tarde por favor." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) populateViewWithInfo{
    if (!selectedPlace.hasDetails) {
        [googleBttn setHidden:YES];
        [starsView setHidden:YES];
        [iconsView setHidden:YES];
        [ratePlace setHidden:YES];
    }else{
        NSString *mapsUrl = [NSString stringWithFormat:@"%@", selectedPlace.googleMapsLocation];
        if (!([mapsUrl isEqualToString:@""])) {
            [googleBttn setHidden:NO];
        }
        starsView.rating = selectedPlace.rating;
        [iconsView setIconsfromString:selectedPlace.infoDetail];
        
        if (!([selectedPlace.suggestions isEqualToString:@""])) {
            hasSuggestions = YES;
        }
        
        if (!(([selectedPlace.descriptionLocation isEqualToString:@""]) || ([selectedPlace.engDescription isEqualToString:@""]))){
            hasDescription = YES;
        }
    }
    addressLbl.text = selectedPlace.address;
    scheduleLbl.text = selectedPlace.schedule;
    phoneLbl.text = selectedPlace.phoneNumber;
    [imagesView reloadData];
    [detailTbl performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

@end
