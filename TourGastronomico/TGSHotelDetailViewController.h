//
//  TGSHotelDetailViewController.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 27/4/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import "TGSCity.h"
#import "TGSPlace.h"
#import "TGSPlacesRequests.h"
#import "LFMRateView.h"
#import "TGSIconsView.h"
#import "TGSImageFullScreenViewController.h"
#import "Haneke.h"

@interface TGSHotelDetailViewController : UIViewController <RateViewDelegate>{
    
    __weak IBOutlet UILabel *hotelName;
    __weak IBOutlet UILabel *addressLbl;
    __weak IBOutlet UILabel *contactLbl;
    __weak IBOutlet UIButton *googleMapsBttn;
    __weak IBOutlet UIImageView *hotelImage;
    __weak IBOutlet TGSIconsView *infoImages;
    __weak IBOutlet LFMRateView *hotelStars;
    __weak IBOutlet UIButton *returnBttn;
}

@property (nonatomic, strong) TGSPlace *selectedPlace;

- (void) successfulPlace: (TGSPlace *) place;
- (void) errorInConnection;
- (IBAction)googleMaps:(id)sender;
- (IBAction)returnToView:(id)sender;

@end
