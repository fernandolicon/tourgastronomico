//
//  ViewController.h
//  TourGastronomico
//
//  Created by Luis Fernando Mata on 13/3/15.
//  Copyright (c) 2015 LuisFernandoMataLicon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TGSCity.h"
#import "SVProgressHUD.h"

@interface TGSCityViewController : UIViewController{
    
    __weak IBOutlet UILabel *cityLabel;
    __weak IBOutlet UIButton *restaurantButton;
    __weak IBOutlet UIButton *hotelsButtons;
    __weak IBOutlet UIImageView *bannerImage;
}

@property (strong, nonatomic) TGSCity *citySelected;

- (IBAction)listPlaces:(id)sender;
- (IBAction)chooseCity:(id)sender;

@end

